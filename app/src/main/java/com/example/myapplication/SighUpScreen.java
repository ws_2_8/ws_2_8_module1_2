package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.service.autofill.RegexValidator;
import android.util.Patterns;
import android.view.View;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;

public class SighUpScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sigh_up_screen);
    }

    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_reg:
                    if(Validation()){
                        SharedPreferences sharedPreferences = getSharedPreferences("SET", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean("REG", true);

                        Intent intent = new Intent(SighUpScreen.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                break;
        }
    }

    boolean Validation(){
        AwesomeValidation validation = new AwesomeValidation(ValidationStyle.BASIC);
        validation.addValidation(this, R.id.et_name1, RegexTemplate.NOT_EMPTY, R.string.error_empty);
        validation.addValidation(this, R.id.et_name2, RegexTemplate.NOT_EMPTY, R.string.error_empty);
        validation.addValidation(this, R.id.et_mail, RegexTemplate.NOT_EMPTY, R.string.error_empty);
        validation.addValidation(this, R.id.et_pass1, RegexTemplate.NOT_EMPTY, R.string.error_empty);
        validation.addValidation(this, R.id.et_pass2, RegexTemplate.NOT_EMPTY, R.string.error_empty);

        validation.addValidation(this, R.id.et_mail, Patterns.EMAIL_ADDRESS, R.string.error_email);

        validation.addValidation(this, R.id.et_pass2, R.id.et_pass1, R.string.error_pass);

        return validation.validate();
    }
}