package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

public class LaunchScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch_screen);

        new Handler().postDelayed(() -> {
            SharedPreferences sharedPreferences = getSharedPreferences("SET", MODE_PRIVATE);
            if(sharedPreferences.getBoolean("REG", false) == false)
                startActivity(new Intent(LaunchScreen.this, SignInScreen.class));
            else
                startActivity(new Intent(LaunchScreen.this, MainActivity.class));

            finish();
        }, 2000);
    }
}