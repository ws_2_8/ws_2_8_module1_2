package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;

public class SignInScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in_screen);
    }

    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_logIn:
                    if(Validation()){
                        Intent intent = new Intent(SignInScreen.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                break;
            case R.id.btn_reg:
                SharedPreferences sharedPreferences = getSharedPreferences("SET", MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean("REG", true);

                Intent intent = new Intent(SignInScreen.this, SighUpScreen.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    boolean Validation(){
        AwesomeValidation validation = new AwesomeValidation(ValidationStyle.BASIC);
        validation.addValidation(this, R.id.et_login, RegexTemplate.NOT_EMPTY, R.string.error_empty);
        validation.addValidation(this, R.id.et_pass, RegexTemplate.NOT_EMPTY, R.string.error_empty);

        validation.addValidation(this, R.id.et_login, Patterns.EMAIL_ADDRESS, R.string.error_email);

        return validation.validate();
    }
}